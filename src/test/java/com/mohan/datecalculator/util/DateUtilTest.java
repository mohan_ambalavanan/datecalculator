package com.mohan.datecalculator.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ambalavanan mohan on 26/04/2016.
 */
public class DateUtilTest {
    @Test
    public void isLeapYear() throws Exception {
        assertThat(DateUtil.isLeapYear(2000),is(true));
        assertThat(DateUtil.isLeapYear(1996),is(true));
        assertThat(DateUtil.isLeapYear(2004),is(true));
        assertThat(DateUtil.isLeapYear(1600),is(true));
        assertThat(DateUtil.isLeapYear(1700),is(false));
        assertThat(DateUtil.isLeapYear(1900),is(false));
        assertThat(DateUtil.isLeapYear(2100),is(false));
        ;

    }

}