package com.mohan.datecalculator.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * Created by ambalavanan mohan on 25/04/2016.
 */
public class PreconditionsUtilTest {
    @Test(expected = IllegalArgumentException.class)
    public void givenInvalidLengthcheckValidLengthShouldThrow() throws Exception {
        PreconditionsUtil.checkValidLength(new String[]{},1,"");
    }

    @Test
    public void givenValidLengthcheckValidLengthShouldReturnSameArray() throws Exception {
        String[] testString = new String[] {"Test","Test1"};
        assertThat(PreconditionsUtil.checkValidLength(testString,2,""),is(testString));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNullcheckNullShouldThrow() throws Exception {
        PreconditionsUtil.checkNull(null,"");

    }
    @Test
    public void givenValidObjectcheckNullShouldReturnValidObject() throws Exception {
        Object obj = new Object();
        assertThat(PreconditionsUtil.checkNull(obj,""),is(obj));

    }

    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyStringcheckEmptyStringShouldThrow() throws Exception {
        PreconditionsUtil.checkEmptyString("","");

    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNullcheckEmptyStringShouldThrow() throws Exception {
        PreconditionsUtil.checkEmptyString(null,"");

    }

    @Test
    public void givenValidStringShouldReturnString() throws Exception {
        String testString = "ABC";
        assertThat(PreconditionsUtil.checkEmptyString(testString,""),is(testString));

    }

}