package com.mohan.datecalculator;

import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Compares the test aga
 * Created by ambalavanan mohan on 26/04/2016.
 */
@RunWith(Parameterized.class)
public class DaysCalculatorTest {

    private String fromDate;
    private String toDate;

    public DaysCalculatorTest(String dateRange) {
        this.fromDate = dateRange.split(",")[0];
        this.toDate = dateRange.split(",")[1];


    }

    static List<String> parseCsvFile(String fileName) throws Exception{
        List<String> dateRanges = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                if (!line.isEmpty()) {
                    dateRanges.add(line);
                }
            }
        }
        return dateRanges;
    }
    @Parameterized.Parameters
    public static Collection dateRanges() throws Exception {
        List<String> dateRanges = new ArrayList<>();
        dateRanges.addAll(parseCsvFile("src/test/resources/test.csv"));
        dateRanges.addAll(parseCsvFile("src/test/resources/random.csv"));
        return dateRanges;
    }

    @Test
    public void testCalculateNoOfDaysComparingWithJodaTimeNoOfDays() throws Exception {

        DateInfo  fromDateInfo =new DateParser(new DateValidator()).parseDate(fromDate);
        DateInfo toDateInfo = new DateParser(new DateValidator()).parseDate(toDate);
        DaysCalculator daysCalculator =new DaysCalculator(fromDateInfo,toDateInfo);
        int expectedDays = (Days.daysBetween(DateTimeFormat.forPattern("dd/MM/yyyy").parseLocalDate(fromDate),
                DateTimeFormat.forPattern("dd/MM/yyyy").parseLocalDate(toDate)).getDays());

       assertThat(daysCalculator.calcuateNoOfDays(),is((expectedDays-1)));
    }

}