package com.mohan.datecalculator;

import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Created by ambalavanan mohan on 26/04/2016.
 */
public class DateValidatorTest {
    private DateValidator dateValidator = new DateValidator();

    public void assertInvalidDays(int invalidDay) {
        try {
            dateValidator.validateDay(invalidDay);
            fail("dateValidator.validateDay should throw for " + invalidDay);
        } catch (IllegalArgumentException e) {
            //do nothing
        }
    }

    @Test
    public void givenInvalidDayValidateDayShouldThrow() throws Exception {
        assertInvalidDays(0);
        assertInvalidDays(-1);
        assertInvalidDays(32);
        assertInvalidDays(99);
    }

    @Test
    public void givenValidDayValidateDayShouldNotThrow() throws Exception {
        dateValidator.validateDay(1);
        dateValidator.validateDay(29);
        dateValidator.validateDay(30);
        dateValidator.validateDay(31);
    }


    public void assertInvalidMonth(int invalidMonth) {
        try {
            dateValidator.validateMonth(invalidMonth);
            fail("dateValidator.validateMonth should throw for " + invalidMonth);
        } catch (IllegalArgumentException e) {
            //do nothing
        }
    }

    @Test
    public void givenInvalidMonthvalidateMonthShouldThrow() throws Exception {

        assertInvalidMonth(-1);
        assertInvalidMonth(13);
        assertInvalidMonth(14);
    }

    @Test
    public void givenValidMonthvalidateMonthShouldNotThrow() throws Exception {

        dateValidator.validateMonth(1);
        dateValidator.validateMonth(12);
        dateValidator.validateMonth(9);
    }


    public void assertInvalidYear(int invalidYear) {
        try {
            dateValidator.validateYear(invalidYear);
            fail("dateValidator.validateYear should throw for " + invalidYear);
        } catch (IllegalArgumentException e) {
            //do nothing
        }
    }

    @Test
    public void givenInvalidYearvalidateYearShouldThrow() throws Exception {
        assertInvalidYear(1900);
        assertInvalidYear(3000);
    }

    @Test
    public void givenvalidYearvalidateYearShouldNotThrow() throws Exception {
        dateValidator.validateYear(1901);
        dateValidator.validateYear(2999);
    }

    public void assertInvalidFebruaryDays(DateInfo dateInfo) {
        try {
            dateValidator.validateFebruaryDays(dateInfo);
            fail("dateValidator.validateFebruaryDays should throw for " + dateInfo);
        } catch (IllegalArgumentException e) {
            //do nothing
        }
    }

    @Test
    public void givenInvalidValidateFebruaryDaysShouldThrow() throws Exception {
        assertInvalidFebruaryDays(new DateInfo(29, 02, 1977));
        assertInvalidFebruaryDays(new DateInfo(30, 02, 2000));
        assertInvalidFebruaryDays(new DateInfo(29, 02, 1889));
        assertInvalidFebruaryDays(new DateInfo(29, 02, 1900));
        assertInvalidFebruaryDays(new DateInfo(29, 02, 2100));
        assertInvalidFebruaryDays(new DateInfo(29, 02, 1800));


    }

    @Test
    public void givenValidValidateFebruaryDaysShoulNotThrow() throws Exception {
        dateValidator.validateFebruaryDays(new DateInfo(28, 02, 1977));
        dateValidator.validateFebruaryDays(new DateInfo(28, 02, 1889));
        dateValidator.validateFebruaryDays(new DateInfo(30, 03, 2000));
        dateValidator.validateFebruaryDays(new DateInfo(28, 02, 1900));
        dateValidator.validateFebruaryDays(new DateInfo(28, 02, 2100));
        dateValidator.validateFebruaryDays(new DateInfo(28, 02, 1800));
        dateValidator.validateFebruaryDays(new DateInfo(29, 02, 1888));

    }

}