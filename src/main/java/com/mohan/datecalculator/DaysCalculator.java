package com.mohan.datecalculator;


import com.mohan.datecalculator.util.DateUtil;
import static com.mohan.datecalculator.util.DateUtil.DateValues.*;

/***
 * Responsibility of the class is to  provide
 * implementation for calculating the number of days
 * excluding the first day
 * and the last day.
 */
public class DaysCalculator {

    private DateInfo startDate;
    private DateInfo endDate;
    private static final int ONE_DAY = 1;
    private static final int NO_OF_DAYS_NOT_LEAP_YR = 365;
    private static final int NO_OF_DAYS_LEAP_YR = 366;
    private static int daysPerMonth[] = {31, 28, 31, 30, 31, 30,
            31, 31, 30, 31, 30, 31};


    public DaysCalculator(DateInfo startDate, DateInfo endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }


    /**
     * Calculates the number of days from 1900
     * till yesterday based on a proper Date
     * Container object.
     *
     * @param date
     * @return noOfDays
     */
    private int calculateNoOfDaysTillYesterday(DateInfo date) {
        return
                //no of non leap year days from first year
                ((countNumberOfNonLeapYearDaysExceptThisYear(date.getYear())
                        // include additional days for feb for all leap years
                        // except this year
                        + (countNumberOfLeapYrsExceptThisYear(date.getYear()))
                        // include days of  month
                        + daysInThisYearExceptThisMonth(date.getMonth(), date.getYear())
                        // include all days of this month except today
                        + (date.getDay() - 1)
                ));
    }

    private int countNumberOfNonLeapYearDaysExceptThisYear(int year) {
        return ((year - FIRST_YEAR.val()) * NO_OF_DAYS_NOT_LEAP_YR);
    }

    /**
     * Calculates the number of days for all
     * the months in this particular year except
     * this month.
     *
     * @param thisMonth
     * @param year
     * @return alldaysInThisYearExceptThisMonth
     */
    private int daysInThisYearExceptThisMonth(int thisMonth, int year) {
        int index = thisMonth - 1;
        int days = 0;
        for (int i = 0; i < index; i++) {
            days += daysPerMonth[i];

        }
        if (thisMonth > FEB.val() && DateUtil.isLeapYear(year))
            days++;

        return days;

    }

    /**
     * Calculates the number of leap
     * years from first year except the given year
     *
     * @param year
     * @return
     */
    private int countNumberOfLeapYrsExceptThisYear(int year) {
        int count = 0;
        for (int i = FIRST_YEAR.val(); i < year; i++) {
            if (DateUtil.isLeapYear(i))
                count++;
        }
        return count;
    }

    /***
     * Returns the number of days
     * for a given year.
     *
     * @param year
     * @return daysInYear
     */
    private int daysInYear(int year) {
        if (DateUtil.isLeapYear(year))
            return NO_OF_DAYS_LEAP_YR;
        return NO_OF_DAYS_NOT_LEAP_YR;
    }


    /**
     * Computes the number of days between the start date
     * excluding the start and end dates of the
     * given start and end date range.
     * Throws exception on
     * invalid date format.
     *
     * @return numberofDays
     */
    public int calcuateNoOfDays() {
        int noOfStartDays = calculateNoOfDaysTillYesterday(startDate);
        int noOfEndDays = calculateNoOfDaysTillYesterday(endDate);
        if (noOfEndDays > noOfStartDays)
            // we shouldnt include start day to calculate the number
            // of days so add one day
            return (noOfEndDays - (noOfStartDays + ONE_DAY));
        else
            return (noOfStartDays - (noOfEndDays + ONE_DAY));
    }

}
