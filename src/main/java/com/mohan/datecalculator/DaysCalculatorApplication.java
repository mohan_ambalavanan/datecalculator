package com.mohan.datecalculator;


import com.mohan.datecalculator.util.PreconditionsUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for days calculator application
 */
public class DaysCalculatorApplication {
    private static Logger logger = Logger.getLogger(DaysCalculatorApplication.class.getName());

    private static String printUsage() {
        return "=============================================================" +
                "Please execute jar in the following format \n" +
                "java -jar datecalculator 19/03/1999 19/09/1999 \n" +
                " where  first argument is start date and last argument \n" +
                " is end date" +
                "=============================================================";

    }

    public static void main(String[] args) {
        try {
            PreconditionsUtil.checkValidLength(args, 2, "Expected 2 arguments");
            DateParser dateParser = new DateParser(new DateValidator());
            DateInfo fromDate = dateParser.parseDate(args[0]);
            DateInfo toDate = dateParser.parseDate(args[1]);
            DaysCalculator daysCalculator = new DaysCalculator(fromDate,toDate);
            logger.info("No of Days is "+daysCalculator.calcuateNoOfDays() + " days");

        } catch (IllegalArgumentException e) {
            logger.info(printUsage());
            logger.log(Level.SEVERE, e.getMessage(), e);
        }catch (Exception e){
            logger.log(Level.SEVERE,"Unknown exception occured",e);
        }
    }


}
