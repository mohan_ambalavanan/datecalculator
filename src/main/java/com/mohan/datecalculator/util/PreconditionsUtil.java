package com.mohan.datecalculator.util;

/**
 * Created by ambalavanan mohan on 25/04/2016.
 * Simple utility class similar to guava Preconditions
 * to verify correct arguments at the start
 */
public class PreconditionsUtil {

    public static<T> T[] checkValidLength(T[] t, int expectedLength,String msg){
        if(t.length!= expectedLength){
            throw  new IllegalArgumentException(msg);
        }
        return t;
    }
    public static <T> T checkNull(T t,String msg){
       if(t==null)
           throw new IllegalArgumentException(msg);
        return t;
    }

    public static String checkEmptyString(String s,String msg){
        if(s==null||s.isEmpty())
            throw new IllegalArgumentException(msg);
        return  s;
    }
}
