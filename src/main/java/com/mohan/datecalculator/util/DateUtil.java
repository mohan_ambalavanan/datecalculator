package com.mohan.datecalculator.util;


/**
 * An utility class which provides helper methods
 * for calculating the number of
 * days.
 */
public class DateUtil {

    private DateUtil() {
    }


    /**
     * Enum reprensentation for date values
     */
    public static  enum DateValues{
        FEB(2),
        FIRST_YEAR(1901),
        LAST_YEAR(2999);
        private int val;
        DateValues(int val){
            this.val =val;
        }
        public  int val(){
            return  val;
        }
    }

    /**
     * Checks if leap year or not return true
     * if so else false
     * @param year
     * @return
     */
    public static boolean isLeapYear(int year) {
        return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

    }

}
