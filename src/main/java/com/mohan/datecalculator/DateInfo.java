package com.mohan.datecalculator;

import java.io.Serializable;

/**
 * A container to store date as we are interested only  in day,month and
 * year we store only them.
 *
 */
public final class DateInfo implements Serializable {
    private int day;
    private int month;
    private int year;


    public DateInfo(final int day, final int month, final int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }


    public int getMonth() {
        return month;
    }


    public int getYear() {
        return year;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DateInfo{");
        sb.append("day=").append(day);
        sb.append(", month=").append(month);
        sb.append(", year=").append(year);
        sb.append('}');
        return sb.toString();
    }
}
