package com.mohan.datecalculator;

import com.mohan.datecalculator.util.DateUtil;

import static com.mohan.datecalculator.util.DateUtil.DateValues.*;

/**
 * Created by ambalavanan mohan on 26/04/2016.
 */
public class DateValidator {

    private static final int LEAP_YEAR_FEB_DAYS = 29;
    private static final int NON_LEAP_YEAR_FEB_DAYS = 28;


    DateValidator validateDay(int day) {
        if (day <= 0 || day > 31)
            throw new IllegalArgumentException("Invalid day given" + day);
        return this;
    }

    DateValidator validateMonth(int month) {
        if (month < 1 || month > 12)
            throw new IllegalArgumentException("Invalid month given : "
                    + month);
        return this;
    }

    DateValidator validateFebruaryDays(DateInfo dateInfo) {
        if (dateInfo.getMonth() != FEB.val())
            return this;

        if (DateUtil.isLeapYear(dateInfo.getYear())) {
            if (dateInfo.getDay() > LEAP_YEAR_FEB_DAYS) {
                throw new IllegalArgumentException("Invalid date given :" + dateInfo);
            }
        } else if (dateInfo.getDay() > NON_LEAP_YEAR_FEB_DAYS) {
            throw new IllegalArgumentException("Invalid date given :" + dateInfo);
        }
        return this;
    }


    DateValidator validateYear(int year) {
        if (year < FIRST_YEAR.val() || year > LAST_YEAR.val())
            throw new IllegalArgumentException("Invalid year format :" + year);
        return this;

    }


    public DateInfo validate(DateInfo dateInfo) {
        validateDay(dateInfo.getDay()).
                validateMonth(dateInfo.getMonth())
                .validateFebruaryDays(dateInfo)
                .validateYear(dateInfo.getYear());
        return dateInfo;
    }
}
