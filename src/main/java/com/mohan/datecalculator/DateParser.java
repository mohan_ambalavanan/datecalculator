package com.mohan.datecalculator;

import com.mohan.datecalculator.util.PreconditionsUtil;

import java.text.ParseException;
import java.util.Arrays;

/**
 * Naive date parser class which accepts
 * only of format dd/MM/YYYY
 * Parses and validates Date
 *  Created by ambalavanan mohan on 26/04/2016.
 */
public class DateParser {

    private DateValidator dateValidator;

    public DateParser(DateValidator dateValidator) {
        this.dateValidator = dateValidator;
    }


    /**
     * convert dates array to
     * DateInfo object
     * @param dates
     * @return
     */
    DateInfo  makeDateInfo(String[]dates){
        try {
            Integer day = Integer.valueOf(dates[0]);
            Integer month = Integer.valueOf(dates[1]);
            Integer year = Integer.valueOf(dates[2]);
            return  new DateInfo(day,month,year);
        }catch (NumberFormatException ne){
            throw new IllegalArgumentException("Invalid date given :"+ Arrays.toString(dates),ne);
        }
    }

    /**
     * Assumes the given string is of format DD/MM/YYYY.
     * Parses the string and
     * returns it as DateInfo object.
     * Throws exception on invalid date.
     *
     * @param date
     * @return valid dateInfo
     */
    public  DateInfo parseDate(String date)  {
        PreconditionsUtil.checkNull(date, "Given date cannot be null");
        PreconditionsUtil.checkEmptyString(date, "Given date cannot be empty");
        String[] dates = date.split("/");
        PreconditionsUtil.checkValidLength(dates,3,"Given date should of format DD/MM/YYYY for "+date);
        DateInfo dateInfo=makeDateInfo(dates);
        return dateValidator.validate(dateInfo);
    }
}
