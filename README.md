
## Requirements

### To compile and build
* Java 8 
* Maven 3
* Git



### To compile and Run the application

```
  mvn clean install
```

It compiles , executes test and prepares a package and installs executable jar to your 
local maven repository.

datecalculator.jar is packaged and installed into target folder execute  from project folder 
in your shell,
   
   ```
   java -jar target/datecalculator.jar 03/01/1989 03/08/1983
   ```
 
 where,
 
  * datecalculator.jar refers to your local file path 
  * 03/01/1989 is the first argument 
  * 03/08/1983 is the second argument
         
NOTE:         
Application accepts only date of this format dd/MM/YYYY where dd is days MM is month and YYYY is year.
It throws error if other format is provided
